/* --- NRF Timer ---
    Timer with RTC, SD card, Ethernet and display interface
*/
#define DEBUG
#include <Wire.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
//#include "etherShield.h"
#include "ETHER_28J60.h"
#include <SD.h>
#include "Timer.h"
#include <EEPROM.h>
//#include <PinChangeInt.h>
//#include <PinChangeIntConfig.h>

//#include <EtherCard.h>
//#include <tinyFAT.h>

#define SD_CS_PIN   47
#define ETH_CS_PIN  45


#define DS3231_I2C_ADDRESS 0x68

#define ENABLE 27
#define DATA 26
#define CLK 25
#define STROBE 24
#define DP 23

#define CE_PIN  49
#define CSN_PIN 53

#define VALUE1  "0.05 €"
#define VALUE2  "0.1 €"
#define VALUE3  "0.2 €"
#define VALUE4  "0.5 €"
#define VALUE5  "1 €"
#define VALUE6  "2 €"

uint8_t numbers[10] = {0xFC, 0x60, 0XDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0, 0xFE, 0xF6};
uint8_t SDtext[2] = {0xB6, 0x7A};
uint8_t ERRtext[2] = {0x9E, 0xA};
uint8_t spec[1] = {0x02};
// -
String temp = "";

RF24 radio(CE_PIN, CSN_PIN); // Create a Radio

int joystick[1];  // 2 element array holding Joystick readings

const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL };              // Radio pipe addresses for the 2 nodes to communicate.

ETHER_28J60 eth;
static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x23};
static uint8_t ip[4] = {192, 168, 0, 2};
static uint16_t port = 80; // Use port 80 - the standard for HTTP

byte second = 0, minute = 0, hour = 0, dayOfWeek = 0, dayOfMonth = 0, month = 0, year = 0;

Sd2Card card;
SdVolume volume;
SdFile root;
Timer intTimer;
//
File myFile;
const int chipSelect = 47;

struct cas_s {
  int second, minute, hour;
  int dayOfWeek, dayOfMonth, month, year;
} cas;

struct kanal_s {
  int c1, c2, c3, c4, c5, c6;
} kanal;

uint32_t timer = 0;

uint8_t blink=1;

uint16_t hraci_cas=0;

ISR (PCINT1_vect) // handle pin change interrupt for A0 to A5 here
 {Serial.println("DSFDF");
     if(digitalRead(A0))
      Serial.println("ISR A0");
     else if(digitalRead(A1))
      Serial.println("ISR A1");
     else if(digitalRead(A2))
      Serial.println("ISR A2");
     else if(digitalRead(A3))
      Serial.println("ISR A3");
     else if(digitalRead(A4))
      Serial.println("ISR A4");
     else if(digitalRead(A5))
      Serial.println("ISR A5");
 }  

void setup() {
  pinMode(ENABLE, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(STROBE, OUTPUT);

  pinMode(DP, OUTPUT);
  digitalWrite(DP, HIGH);
  Wire.begin();
  // DS3231 seconds, minutes, hours, day, date, month, year
  //  setDS3231time(30, 52, 21, 4, 26, 11, 14);
  set1Hz();

  pinMode(19, INPUT);
  digitalWrite(19, HIGH);
  attachInterrupt(4, SecondInterrupt, FALLING);

  pinMode(A0, INPUT);
  digitalWrite(A0, HIGH);
  pinMode(A1, INPUT);
  digitalWrite(A1, HIGH);
  pinMode(A2, INPUT);
  digitalWrite(A2, HIGH);
  pinMode(A3, INPUT);
  digitalWrite(A3, HIGH);
  pinMode(A4, INPUT);
  digitalWrite(A4, HIGH);
  pinMode(A5, INPUT);
  digitalWrite(A5, HIGH);

  //  intTimer.every(250, readChannelState);

/*  pciSetup(A0);
  pciSetup(A1);
  pciSetup(A2);
  pciSetup(A3);
  pciSetup(A4);
  pciSetup(A5);*/
/*
  attachInterrupt(A0, kanal0, FALLING);
  attachInterrupt(A1, kanal0, FALLING);
  attachInterrupt(A2, kanal0, FALLING);
  attachInterrupt(A3, kanal0, FALLING);
  attachInterrupt(A4, kanal0, FALLING);
*/
 Serial.begin(57600);
 Serial.println("Timer pro strelnici NRF");

  radio.begin();
  radio.setAutoAck(1);                    // Ensure autoACK is enabled
  radio.enableAckPayload();               // Allow optional ack payloads
  radio.setRetries(0,15);                 // Smallest time between retries, max no. of retries
  radio.setPayloadSize(1);                // Here we are sending 1-byte payloads to test the call-response speed
  radio.openWritingPipe(pipes[1]);        // Both radios listen on the same pipes by default, and switch when writing
  radio.openReadingPipe(1,pipes[0]);
  radio.startListening();                 // Start listening
  radio.printDetails();                   // Dump the configuration of the rf unit for debugging

  radio.openWritingPipe(pipes[1]);
       radio.openReadingPipe(1,pipes[0]);
       radio.startListening();
  

 Serial.println("init eth naleduje\r\n\r\n");
  //eth.setup(mac, ip, port);

  
  Serial.println("NRF TIMER\r\n--------------\n\r\n\r");


  //SDcardInfo();

  hraci_cas=224;
timer=hraci_cas;
  /*
    //  EEPROM.write(0,10);
    //  EEPROM.write(1,10);
    //  EEPROM.write(2,10);
    //  EEPROM.write(3,10);
    //  EEPROM.write(4,10);
    //  EEPROM.write(5,10);
  */
  kanal.c1 = EEPROM.read(0);
  kanal.c2 = EEPROM.read(1);
  kanal.c3 = EEPROM.read(2);
  kanal.c4 = EEPROM.read(3);
  kanal.c5 = EEPROM.read(4);
  kanal.c6 = EEPROM.read(5);
}

void loop() {

  char* params;
  int logs = 0;
//sei();
//  intTimer.update();

while(1)
{
  byte pipeNo;
  byte gotByte;                                       // Dump the payloads until we've gotten everything
  while( radio.available(&pipeNo)){
    radio.read( &gotByte, 1 );
    if(timer>0)
      gotByte=2;
    else
      gotByte=4;
    radio.writeAckPayload(pipeNo,&gotByte, 1 );    
 }

}

  //Serial.println("odeslano\r\n") ;
  /*
    if (params=eth.serviceRequest())
    {
      Serial.println(params);
      if (strstr(params,"Ch"))
      {
        char* temp;
        temp=strtok(params,"Ch?1=");
        kanal.c1=atoi(temp);
        temp=strtok(NULL,"&2=");
        kanal.c2=atoi(temp);
        temp=strtok(NULL,"&3=");
        kanal.c3=atoi(temp);
        temp=strtok(NULL,"&4=");
        kanal.c4=atoi(temp);
        temp=strtok(NULL,"&5=");
        kanal.c5=atoi(temp);
        temp=strtok(NULL,"&6=");
        kanal.c6=atoi(temp);

        Serial.print("Kanaly: ");
        Serial.print(kanal.c1,DEC);Serial.print(" , ");
        Serial.print(kanal.c2,DEC);Serial.print(" , ");
        Serial.print(kanal.c3,DEC);Serial.print(" , ");
        Serial.print(kanal.c4,DEC);Serial.print(" , ");
        Serial.print(kanal.c5,DEC);Serial.print(" , ");
        Serial.print(kanal.c6,DEC);Serial.print("\r\n\r\n");

        EEPROM.write(0,kanal.c1);
        EEPROM.write(1,kanal.c2);
        EEPROM.write(2,kanal.c3);
        EEPROM.write(3,kanal.c4);
        EEPROM.write(4,kanal.c5);
        EEPROM.write(5,kanal.c6);

        Serial.println("Coins time value saved");

      }
      else if (strstr(params,"clock"))
      {
        char* temp;
        temp=strtok(params,"clock?h=");
        cas.hour=atoi(temp);
        temp=strtok(NULL,"&m=");
        cas.minute=atoi(temp);
        temp=strtok(NULL,"&d=");
        cas.dayOfMonth=atoi(temp);
        temp=strtok(NULL,"&mo=");
        cas.month=atoi(temp);
        temp=strtok(NULL,"&y=");
        cas.year=atoi(temp);
        Serial.print("nastaveno: ");
        Serial.print(cas.hour,DEC);
        Serial.print(":");
        Serial.print(cas.minute,DEC);
        Serial.print(" ");
        Serial.print(cas.dayOfMonth,DEC);
        Serial.print(".");
        Serial.print(cas.month,DEC);
        Serial.print(".");
        Serial.print(cas.year,DEC);
        Serial.print("\r\n\r\n");
        setDS3231time(0,cas.minute,cas.hour,2,cas.dayOfMonth,cas.month,cas.year);
      }
      if((strcmp(params,"setTime")==0))
      {
          readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
          eth.print("<!DOCTYPE html>");
          eth.print("<html><head><title>Time settings</title><meta charset=\"UTF-8\"></head><body>");
          eth.print("<div align=center><h1>Time settings</h1></div><br>");
          eth.print("<br><br><br><i>Please complete the following for set-up real-time</i><br>");
          eth.print("<br><form method=\"GET\" action=\"/clock\" >Time: ");
          eth.print(" <input type=\"text\" name=h size=\"2\" value=");eth.print(hour);eth.print("> : ");
          eth.print("<input type=\"text\" name=m size=\"2\" value=");eth.print(minute);eth.print("> ");
          eth.print("<br>Date: <input type=\"text\" name=d size=\"2\" value=");eth.print(dayOfMonth);eth.print("> . ");
          eth.print("<input type=\"text\" name=mo size=\"2\" value=");eth.print(month);eth.print("> . ");
          eth.print("<input type=\"text\" name=y size=\"2\" value=");eth.print(year);eth.print("> . ");
          eth.print("  <input name=\"submit\" type=\"submit\" value=\"Save\"/></form>");
          eth.print("</body>");
          eth.respond();
      }
      else if ((strcmp(params,"coins")==0))
      {
          eth.print("<!DOCTYPE html>");
          eth.print("<html><head><title>Settings of Timer</title><meta charset=\"UTF-8\"></head><body>");
          eth.print("<div align=center><h1>Coins value settings</h1></div><br>");
          eth.print("<br><br><i>Please fill this with time corresponding to the value of the coins thrown</i><br>");
          eth.print("<form method=\"GET\" action=\"/Ch\" >");
          eth.print(VALUE1);eth.print(" <input type=\"text\" name=1 size=\"2\" value=");eth.print(kanal.c1);eth.print("> min<br>");
          eth.print(VALUE2);eth.print(" <input type=\"text\" name=2 size=\"2\" value=");eth.print(kanal.c2);eth.print("> min<br>");
          eth.print(VALUE3);eth.print(" <input type=\"text\" name=3 size=\"2\" value=");eth.print(kanal.c3);eth.print("> min<br>");
          eth.print(VALUE4);eth.print(" <input type=\"text\" name=4 size=\"2\" value=");eth.print(kanal.c4);eth.print("> min<br>");
          eth.print(VALUE5);eth.print(" <input type=\"text\" name=5 size=\"2\" value=");eth.print(kanal.c5);eth.print("> min<br>");
          eth.print(VALUE6);eth.print(" <input type=\"text\" name=6 size=\"2\" value=");eth.print(kanal.c6);eth.print("> min<br><br>");
          eth.print("  <input name=\"submit\" type=\"submit\" value=\"Save\"/></form>");

          eth.print("</body>");
          eth.respond();
      }
      else
      {
          readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);
          eth.print("<!DOCTYPE html>");
          eth.print("<html><head><title>Settings of Timer</title><meta charset=\"UTF-8\"></head><body>");
          eth.print("<div align=center><h1>TIMER - inserted coins</h1></div><br><br>");
          eth.print("Actual time: ");
          eth.print(hour);
          eth.print(":");
          eth.print(minute);
          eth.print(":");
          eth.print(second);
          eth.print("<br><br>");
          eth.print("<a href=/>Reload</a><br>");
          eth.print("<a href=/setTime>Time setting</a><br>");
          eth.print("<a href=/coins>Coins value setting</a><br><br>");
          eth.print("<b>Last a few logs of inserted coins:</b><br><br>");
          // re-open the file for reading:
          myFile = SD.open("log.txt");
          if (myFile) {
            Serial.println("test.txt:");
          char fromSD[1000];
            // read from the file until there's nothing else in it:
            while (myFile.available() && logs <100) {
              sprintf(fromSD,"%c",myFile.read());
              if (strcmp(fromSD,".")==0)
                eth.print("<br>");
              else
                eth.print(fromSD);
                logs++;
            }
            // close the file:
            myFile.close();
          } else {
            // if the file didn't open, print an error:
            Serial.println("error opening test.txt");
          }

          eth.print("</body>");
          eth.respond();
      }

    }
  */
}

void kanal0() {
  Serial.println("sdfsdf");
}

void SecondInterrupt() {
  // readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month, &year);

  if(blink)
    blink=0;
  else
    blink=1;
  
  if ((timer == 0) && (blink==1))
  {
    shift8bits(numbers[0]);
    shift8bits(numbers[0]);
    shift8bits(numbers[0]);
    shift8bits(numbers[0]);
  }
  else if ((timer == 0) && (blink==0))
  {
    shift8bits(0);
    shift8bits(0);
    shift8bits(0);
    shift8bits(0);
  } 
  else  if (timer > 59)
  {
    int minute = timer / 60;
    int second = timer - minute * 60;
    shift8bits(numbers[minute / 10]);
    shift8bits(numbers[minute % 10]);
    shift8bits(numbers[second / 10]);
    shift8bits(numbers[second % 10]);
  }
  else
  {
    shift8bits(numbers[0]);
    shift8bits(numbers[0]);
    shift8bits(numbers[timer / 10]);
    shift8bits(numbers[timer % 10]);
  }

  if(timer > 0)
    timer--;
  
}

void readChannelState() {
  /*Serial.println("CHANNEL 0: ");
    Serial.println( analogRead(A0));
    Serial.println("CHANNEL 1: ");
    Serial.println( analogRead(A1));
  */
  Serial.println("zacatek TIMER ISR");
  joystick[0] = 245;
  joystick[1] = 247 % 10;
  radio.write( joystick, sizeof(joystick) );

  if (analogRead(A0) < 500 )
    timer = timer + kanal.c1;
  else if (analogRead(A1) < 500 )
    timer = timer + kanal.c2;
  else if (analogRead(A2) < 500 )
    timer = timer + kanal.c3;
  else if (analogRead(A3) < 500 )
    timer = timer + kanal.c4;
  else if (analogRead(A4) < 500 )
    timer = timer + kanal.c5;
  else if (analogRead(A5) < 500 )
    timer = timer + kanal.c6;

  /* Serial.print("Added time: ");
    Serial.print(timer,DEC);
    Serial.print("\r\n");*/
  Serial.println("konec TIMER ISR");
}

void A0Interrupt()
{
  Serial.println("in ISR CHANNEL 0: ");
  Serial.println( analogRead(A0));
}


void shift8bits (uint8_t data)
{
  digitalWrite(ENABLE, HIGH);
  for (int i = 0; i < 8; i++)
  {
    if (data & 1) {
      digitalWrite(DATA, HIGH);
    }
    else {
      digitalWrite(DATA, LOW);
    }
    digitalWrite(CLK, HIGH);
    digitalWrite(CLK, LOW);
    data >>= 1;
  }
  digitalWrite(STROBE, HIGH);
  digitalWrite(STROBE, LOW);
  digitalWrite(ENABLE, LOW);

}

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val) {
  return ( (val / 10 * 16) + (val % 10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val) {
  return ( (val / 16 * 10) + (val % 16) );
}

void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year) {
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}

void set1Hz (void)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0x0E); // set next input to start at the seconds register
  Wire.write(0);
  Wire.endTransmission();
}

void readDS3231time(byte *second, byte *minute, byte *hour, byte *dayOfWeek, byte *dayOfMonth, byte *month, byte *year) {
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}

void SDcardInfo (void)
{
  Serial.print("\nInitializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output
  // or the SD library functions will not work.
  pinMode(chipSelect, OUTPUT);

  digitalWrite(chipSelect, HIGH);
  SD.begin(chipSelect);
  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  while (!card.init(SPI_HALF_SPEED, chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("* is a card is inserted?");
    Serial.println("* Is your wiring correct?");
    Serial.println("* did you change the chipSelect pin to match your shield or module?");
    shift8bits(SDtext[0]);
    shift8bits(SDtext[1]);
    shift8bits(ERRtext[0]);
    shift8bits(ERRtext[1]);
  }

  // print the type of card
  Serial.print("\nCard type: ");
  switch (card.type()) {
    case SD_CARD_TYPE_SD1:
      Serial.println("SD1");
      break;
    case SD_CARD_TYPE_SD2:
      Serial.println("SD2");
      break;
    case SD_CARD_TYPE_SDHC:
      Serial.println("SDHC");
      break;
    default:
      Serial.println("Unknown");
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
    return;
  }

  // print the type and size of the first FAT-type volume
  uint32_t volumesize;
  Serial.print("\nVolume type is FAT");
  Serial.println(volume.fatType(), DEC);
  Serial.println();

  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
  volumesize *= 512;                            // SD card blocks are always 512 bytes
  Serial.print("Volume size (bytes): ");
  Serial.println(volumesize);
  Serial.print("Volume size (Kbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  Serial.print("Volume size (Mbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);


  Serial.println("\nFiles found on the card (name, date and size in bytes): ");
  root.openRoot(volume);

  // list all files in the card with date and size
  root.ls(LS_R | LS_DATE | LS_SIZE);

  // re-open the file for reading:
  myFile = SD.open("test.txt");
  if (myFile) {
    Serial.println("test.txt:");

    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      Serial.write(myFile.read());
    }
    // close the file:
    myFile.close();
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }


  myFile = SD.open("log.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to test.txt...");
    myFile.println("testing 1, 2, 3.\r\n");
    // close the file:
    myFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  myFile = SD.open("time.txt");
  if (myFile) {
    Serial.println("time.txt:");

    char data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.second = temp.toInt();
    Serial.println(cas.second);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.minute = temp.toInt();
    Serial.println(cas.minute);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.hour = temp.toInt();
    Serial.println(cas.hour);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.dayOfWeek = temp.toInt();
    Serial.println(cas.dayOfWeek);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.dayOfMonth = temp.toInt();
    Serial.println(cas.dayOfMonth);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.month = temp.toInt();
    Serial.println(cas.month);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.year = temp.toInt();
    Serial.println(cas.year);
    temp = "";
    myFile.read();

    // close the file:
    myFile.close();
  }
}

void readValueFromSD()
{
  myFile = SD.open("value.txt");
  if (myFile) {
    Serial.println("Prectene hodnoty minut pro vhozene mince: \r\n");

    char data = myFile.read();
    //    kanal.c1=data.toInt();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.second = temp.toInt();
    Serial.println(cas.second);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.minute = temp.toInt();
    Serial.println(cas.minute);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.hour = temp.toInt();
    Serial.println(cas.hour);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.dayOfWeek = temp.toInt();
    Serial.println(cas.dayOfWeek);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.dayOfMonth = temp.toInt();
    Serial.println(cas.dayOfMonth);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.month = temp.toInt();
    Serial.println(cas.month);
    temp = "";
    myFile.read();

    data = myFile.read();
    temp += data;
    data = myFile.read();
    temp += data;
    cas.year = temp.toInt();
    Serial.println(cas.year);
    temp = "";
    myFile.read();

    // close the file:
    myFile.close();
  }
}

void software_Reset() // Restarts program from beginning but does not reset the peripherals and registers
{
  asm volatile ("  jmp 0");
}

void pciSetup(byte pin)
{
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
  PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

